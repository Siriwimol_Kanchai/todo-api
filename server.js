const express = require('express')//import library
const app = express()//create express application

let todos =[//todos เป็น Array
  {
      name : 'siriwimol',
      id :1
  },
  {
    name : 'siriwimol',
    id :2
}
]
//SELECT*FROM TODO
app.get('/todos',(req,res)=>{
     res.send(todos)//res.send ตามด้วยข้อมูลที่อยากส่งกลับ
})
//INSERT INTO TODO
app.post('/todos',(req,res)=>{
     let newTodo = {//เป็นการเพิ่มของมูลลงไปใน todos
         name : 'Read a book',
         id : 3
     }
     todos.push(newTodo)
     res.status(201).send()
})
app.listen(3000,()=>{
    console.log('TODO API Started at port 3000')
})